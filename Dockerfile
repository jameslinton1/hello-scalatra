from tomcat:7.0

maintainer jameslinton

copy target/*.war /usr/local/tomcat/webapps/hello-scalatra.war

expose 8080

CMD ["catalina.sh", "run"]